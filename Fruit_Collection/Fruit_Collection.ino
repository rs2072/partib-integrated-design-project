// pins
#define ldr A0  // LDR
#define ls1 A1  // Line sensor
#define ls2 A2
#define ls3 A3
#define ustrig 3  // Ultrasound sensor
#define usecho 2
#define startswitch 6 // Start
#define resetswitch 7 // Reset
#define greenled 8  // LED
#define redled 11
#define amberled 12
#define whiteled 

// libraries
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// Select "port" for motors
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

Servo sensor_servo;                       // Create servo object controlling the sensor door
Servo basket_servo;                       // Create servo object controlling the basket door

// Declare variables
int usduration, ldr_value, pos = 0 /*servo position*/;
volatile uint8_t leftSpeed, rightSpeed;
float f_distance;
volatile boolean sensordoor_open = false;
volatile boolean basketdoor_open = false;

// constant
#define passiveSpeed 130

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);
  
  AFMS.begin();                           // create with the default frequency 1.6KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  leftSpeed = passiveSpeed;
  rightSpeed = passiveSpeed;
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);

  sensor_servo.attach(9);                 // attaches the sensor door servo on pin 9
  basket_servo.attach(10);                // attaches the basket door servo on pin 10
  // initial position: sensor door at front, basket door closing the basket (both closed)

}

void loop() {
  // put your main code here, to run repeatedly:
  
  // read ultrasound sensor (distance)
  noInterrupts();
  digitalWrite(ustrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(ustrig, LOW);
  usduration = pulseIn(usecho, HIGH);
  interrupts();
  f_distance = usduration * 0.034/2;

  if (0 < f_distance <= 5.2) {            // there is a fruit within 5 cm
    
    // stop the robot
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);

    // light up amber LED for 1 sec
    digitalWrite(amberled, HIGH);
    delay(1000);
    digitalWrite(amberled, LOW);
    
    // detect colour
    ldr_value = analogRead(ldr);
    if (ldr_value <= 500) {               // blue block -> unripe fruit
      // red LED >= 5 sec
      digitalWrite(redled, HIGH);
      delay(6000);
      digitalWrite(redled, LOW);
      // open sensor door to 90 degrees to avoid unripe fruit, keep basket door closed
      for (pos = 0; pos <= 90; pos += 1) {  // ~~ position does not match actual angle
        sensor_servo.write(pos);
      }
      sensordoor_open = true;
    } else {  // red block -> ripe fruit
      // green LED >= 5 sec
      digitalWrite(greenled, HIGH);
      delay(6000);
      digitalWrite(greenled, LOW);
      // open both doors and align them to collect the fruit
      for (pos = 0; pos <= 35; pos += 1) {  // ~~
        sensor_servo.write(pos);
      }
      for (pos = 0; pos <= 55; pos += 1) {  // ~~
        basket_servo.write(pos);
      }
      sensordoor_open = true;
      basketdoor_open = true;
    }

    // collect or avoid the fruit
    if (sensordoor_open == true && basketdoor_open == false) { // drive past the fruit
      leftMotor->setSpeed(150);
      rightMotor->setSpeed(150);            // !!!!!!! needs global variable !!!!!!!~
      delay(1000);                          // ~~~ need to test how long should the robot move
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(0);
      // close the sensor door
      for (pos = 90; pos <= 0; pos -= 1) {  // ~~
        sensor_servo.write(pos);
      }
      sensordoor_open = false;
    } else if (sensordoor_open == true && basketdoor_open == true) {  //collect the fruit
      leftMotor->setSpeed(150);
      rightMotor->setSpeed(150);            // !!!!!!! needs global variable !!!!!!!~
      delay(1000);                          // ~~~
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(0);
      // close both doors
      for (pos = 35; pos <= 0; pos -= 1) {  // ~~
        sensor_servo.write(pos);
      }
      for (pos = 55; pos <= 0; pos -= 1) {  // ~~
        basket_servo.write(pos);
      }
      sensordoor_open = false;
      basketdoor_open = false;
    } else if (sensordoor_open == false && basketdoor_open == false) { // continue following the line
      // !!!!!!! continue line following !!!!!!!~ (call line following function?)
      
    }
  }
}
