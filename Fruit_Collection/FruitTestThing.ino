int analogPin = A0; // potentiometer wiper (middle terminal) connected to analog pin 3
                    // outside leads to ground and +5V
float val = 0;  // variable to store the value read
float val1 = 0;

#define trigPin 3
#define echoPin 2
int duration;
float distanceCm;

void setup() {
  Serial.begin(9600);           //  setup serial
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
}

void loop() {
  Serial.print("distance: ");
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distanceCm= duration*0.034/2;
  Serial.println(distanceCm);
  val = analogRead(analogPin);  // read the input pin
  delay(1);
  digitalWrite(13, LOW);
  delay(1000);
  val1 = analogRead(analogPin);
  delay(1);
  digitalWrite(13, HIGH);
  delay(1000);
  Serial.println(val-val1);          // debug value
}