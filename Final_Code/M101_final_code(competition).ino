//Libraries
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

//Set shield to default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

//Set up left and right motor
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

Servo sensor_servo;                       // Create servo object controlling the sensor door
Servo basket_servo;                       // Create servo object controlling the basket door

//Pins
#define ldr A1
#define ls1 A1 //left line sensor
#define ls2 A2 //middle line sensor
#define ls3 A3 //right line sensor
#define ustrig 3
#define usecho 2
#define startswitch 6
#define resetswitch 7
#define greenled 8
#define redled 11
#define amberled 12
#define whiteled 13

//Other constants
#define redMin 340                        //minimum threshold for line sensors to be red
#define redMax 500                        //maximum threshold for line sensors to be red
#define tunnelTime 600                    //time for the rest of the robot to go through the tunnel
#define turningTime 10000                 //time for the robot to turn to face the line after the tunnel
#define passiveSpeed 150                  //default speed of both motors
#define slowWheel 20                      //speed of the slow wheel when turning
#define fastWheel 150                     //speed of the fast wheel when turning
#define lhOffset 00                       //offset for left line sensor
#define rhOffset 150                      //offset for right line sensor as it give higher values than expected


//Variables
volatile boolean started = false;         //tag to say if the robot has started
volatile uint8_t leftSpeed, rightSpeed;   //speed variables of the left and right motors
volatile boolean forwardDirection = true; //says whether the robot is moving forward or backward
volatile int redLines = 0;                //the number of red lines robot has encountered
volatile boolean onBlack;                 //says if the robot is ona black are of the table
volatile boolean goStraight = false;      //tag to go straight when on a black area of the table
unsigned long whiteLine = 0;              //records time of when the robot came across a white line
volatile boolean amberValue;              //holds the value of the amber LED 
unsigned long amberLast;                  //hold value of millis() last time amber led changed
int usduration;                           // value received from ultrasound sensor
int ldr_value, ldr_value0, ldr_value1;    // value read from LDR
int pos = 0;                              // denotes servo position
float f_distance;                         // robot-to-fruit distance at front
volatile boolean sensordoor_open = false; // says the sensor door is open or not
volatile boolean basketdoor_open = false; // says the basket door is open or not


void setup() {
  //set up serial and AFMS with default frequency
  Serial.begin(9600);
  AFMS.begin();

  // set up LEDs
  amberValue = false;
  pinMode(amberled, OUTPUT);
  digitalWrite(amberled, LOW);

  pinMode(redled, OUTPUT);
  digitalWrite(redled, LOW);
  pinMode(greenled, OUTPUT);
  digitalWrite(greenled, LOW);
  pinMode(whiteled, OUTPUT);
  digitalWrite(whiteled, LOW);
  
  //Connect button interrupts
  attachInterrupt(digitalPinToInterrupt(startswitch), startFunc, RISING);
  attachInterrupt(digitalPinToInterrupt(resetswitch), resetFunc, RISING);

  //for testing without buttons ~
  //startFunc();

  sensor_servo.attach(9);                 // attaches the sensor door servo on pin 9
  basket_servo.attach(10);                // attaches the basket door servo on pin 10
  // initial position: sensor door at front, basket door closing the basket (both closed)
  sensor_servo.write(82);
  basket_servo.write(90);
    
  //Set motors to zero
  leftSpeed = 0;
  rightSpeed = 0;  
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
}

void loop() {
  //if the robot has started then do things
  if (started) {
    lineFollow();
    fruit_collection();
    if (millis() - amberLast >= 250) {
      amberLast = millis();
      amberValue = amberValue ^ 1;
      digitalWrite(amberled, amberValue);
    }
  } else {
    if (leftSpeed != 0 || rightSpeed != 0) {
      leftSpeed = 0;
      rightSpeed = 0;
      leftMotor->setSpeed(leftSpeed);
      leftMotor->run(RELEASE);
      rightMotor->setSpeed(rightSpeed);
      rightMotor->run(RELEASE);
    }
  }
}

void startFunc() {
  //intialise some variables
  forwardDirection = true;
  started = true;
  redLines = 0;
}

void resetFunc() {
  //reset some variables
  forwardDirection = true;
  //stop amber light
  amberValue = false;
  digitalWrite(amberled, amberValue);
  started = false;
}

boolean isRed(int value) {
  //returns  boolean dependant on if the sensor value is in red range
  if (value >= redMin && value <= redMax) {
    return true;
  } else {
    return false;
  }
}

boolean isWhite(int value) {
  //returns  boolean dependant on if the sensor value is in white range
  if (value <= redMin) {
    return true;
  } else {
    return false;
  }
}

boolean isBlack(int value) {
  //returns  boolean dependant on if the sensor value is in black range
  if (value >= redMax) {
    return true;
  } else {
    return false;
  }
}

void lineFollow() {
  //reads the line sensors
  int left, mid, right;
  left = analogRead(ls1);
  mid = analogRead(ls2);
  right = analogRead(ls3);
  left = left - lhOffset;
  right = right - rhOffset;

  //checks which sensors are active in different ways
  if (isBlack(left) && isWhite(mid) && isBlack(right)) {
    //going straight BWB
    moveForward();
    goStraight = false;
    onBlack = false;
  } else if(isBlack(left) && isBlack(mid) && isBlack(right)) {
    //on black ground BBB
    onBlack = true;
    if (goStraight){
      if ((millis() > whiteLine + tunnelTime) && (millis() < whiteLine + tunnelTime + turningTime)) {
        //need to turn to get back on track after going through the tunnel
        fastTurnLeft();       
      } else {
        //just need to go straight forward/backward
        moveForward();
      }
    } else {
      //turn left as most likely, the robot went off on the right
      turnRight();
    }
  } else if (isRed(left) && isRed(right)) {
    //straight across red line, front and back of fruit box RRR
    //ignore middle value as the white tape under the red tape gives strange values
    goStraight = true;
    if (onBlack == true) {
      redLines += 1;
      digitalWrite(redled, HIGH);
      if (redLines == 1) {
        leftSpeed = 0;
        rightSpeed = 0;
        leftMotor->setSpeed(leftSpeed);
        rightMotor->setSpeed(rightSpeed);
        leftMotor->run(RELEASE);
        rightMotor->run(RELEASE);
        leftMotor->run(BACKWARD);
        rightMotor->run(FORWARD);
        forwardDirection = false;
        //open the door so that the fruit can leave
        basket_servo.write(90); 
        delay(50); //ensure that the servo has fully opened
        moveForward();
        int i;
        for (i = 0; i<22; i++) {
          digitalWrite(amberled, HIGH);
          delay(250);
          digitalWrite(amberled, LOW);
          delay(250);
        }
        turnLeft();
        delay(100);
      }
    }
    onBlack = false;
  } else if (isBlack(left) && isWhite(mid) && isWhite(right)) {
    //finished loop, need to get back into tunnel BWW
    turnRight();
  } else if (isWhite(left) && isWhite(mid) && isWhite(right)) {
    digitalWrite(greenled, HIGH);
    //Straight across line - at start and just out of tunnel WWW
    if (millis() < 20000) {
      moveForward();
    } else if (millis() > 75000) {
      turnRight();
    } else {
      whiteLine = millis();
      onBlack = false;
      if (forwardDirection) {
        goStraight = true;
        fastTurnLeft();
      } else {
        delay(250);
        leftSpeed = 0;
        rightSpeed = 0;
        leftMotor->setSpeed(leftSpeed);
        rightMotor->setSpeed(rightSpeed);
        leftMotor->run(RELEASE);
        rightMotor->run(RELEASE);
        digitalWrite(amberled, LOW);
        started = false;
      }
    }
    delay(200);
  } else if (isWhite(left) && isBlack(mid) && isWhite(right)) {
    //WBW bear right
    turnRight();
    delay(100);
  } else if (isWhite(left) && isBlack(mid) && isBlack(right)) {
    //need to turn left WBB
    turnLeft();
  } else if (isBlack(left) && isBlack(mid) && isWhite(right)) {
    //need to turn right BBW
    turnRight();
    delay(5);
  } else if (isWhite(left) && isWhite(mid) && isBlack(right)) {
    //Drifting to the right WWB
    turnLeft();
  } else {
    //nothing else should happen so print the values for debugging
    //commented out as serial might slow down the polling of the sensors
    /*Serial.print(left);
    Serial.print(", ");
    Serial.print(mid);
    Serial.print(", ");
    Serial.print(right);*/
  }
  
}

void moveForward() {
  //function to move the robot forward
  if ((leftSpeed != passiveSpeed) || (rightSpeed != passiveSpeed)) {
      leftSpeed = passiveSpeed;
      rightSpeed = passiveSpeed;
      leftMotor->setSpeed(leftSpeed); //add offset when needed so that the wheel speeds are more similar
      rightMotor->setSpeed(rightSpeed);
   }
}

void turnLeft() {
  //function to turn the robot left
  //it works by increasing the speed of the right wheel
  if ((leftSpeed != slowWheel) || (rightSpeed != fastWheel)) {
      leftSpeed = slowWheel;
      rightSpeed = fastWheel;
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
   }
}

void turnRight() {
  //function to turn the robot right
  //it works by decreasing the speed of the right wheel
  if ((leftSpeed != fastWheel) || (rightSpeed != slowWheel)) {
      leftSpeed = fastWheel;
      rightSpeed = slowWheel;
      leftMotor->setSpeed(leftSpeed+20);
      rightMotor->setSpeed(rightSpeed);
   }
}

void fastTurnLeft() {
  //turn to the right with a sharp cornering speed
  if ((leftSpeed != 50) || (rightSpeed != 255)) {
      leftSpeed = 50;
      rightSpeed = 255;
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
   }
}


void fruit_collection() {
  // read ultrasound sensor (distance)
  noInterrupts();
  digitalWrite(ustrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(ustrig, LOW);
  usduration = pulseIn(usecho, HIGH);
  interrupts();
  f_distance = usduration * 0.034/2;
  Serial.print("distance: ");
  Serial.println(f_distance);

  if ((5 < f_distance) && (f_distance <= 6)) {            // there is a fruit within 5 cm
    // stop the robot
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);

    // light up amber LED for 1 sec
    digitalWrite(amberled, HIGH);
    delay(1000);
    digitalWrite(amberled, LOW);

    //turn on white LED
    digitalWrite(whiteled, HIGH);
    delay(1000);
    // detect colour
    ldr_value0 = analogRead(ldr);
    delay(1);
    //turn off white LED
    digitalWrite(whiteled, LOW);
    delay(1000);
    ldr_value1 = analogRead(ldr);
    ldr_value = ldr_value0 - ldr_value1;
    if (ldr_value <= 500) {               // blue block -> unripe fruit
      // red LED >= 5 sec
      digitalWrite(redled, HIGH);
      delay(6000);
      digitalWrite(redled, LOW);
      // open sensor door to 90 degrees to avoid unripe fruit, keep basket door closed
      sensor_servo.write(165);
      sensordoor_open = true;
      basket_servo.write(160);
      basketdoor_open = false;
    } else {  // red block -> ripe fruit
      // green LED >= 5 sec
      digitalWrite(greenled, HIGH);
      delay(6000);
      digitalWrite(greenled, LOW);
      // open both doors and align them to collect the fruit
      sensor_servo.write(115);
      basket_servo.write(90);
      sensordoor_open = true;
      basketdoor_open = true;
    }

    // collect or avoid the fruit
    if (sensordoor_open == true && basketdoor_open == false) { // drive past the fruit
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
      delay(2000);
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(0);
      // close the sensor door
      sensor_servo.write(82);
      sensordoor_open = false;
    } else if (sensordoor_open == true && basketdoor_open == true) {  //collect the fruit
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
      delay(2000);
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(0);
      // close both doors
      sensor_servo.write(82);
      basket_servo.write(160);
      sensordoor_open = false;
      basketdoor_open = false;
    } else if (sensordoor_open == false && basketdoor_open == false) { // continue following the line
      lineFollow();
    }
  } else {
    sensor_servo.write(82);
    basket_servo.write(160);
  }
}
