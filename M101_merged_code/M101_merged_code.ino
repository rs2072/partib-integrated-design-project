//Libraries
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

//Set shield to default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

//Set up left and right motor
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

Servo sensor_servo;                       // Create servo object controlling the sensor door
Servo basket_servo;                       // Create servo object controlling the basket door

//Pins
#define ldr A0
#define ls1 A1 //left line sensor
#define ls2 A2 //middle line sensor
#define ls3 A3 //right line sensor
#define ustrig 3
#define usecho 2
#define startswitch 6
#define resetswitch 7
#define greenled 8
#define redled 11
#define amberled 12
#define whiteled 13

//Other constants
#define redMin 350                        //minimum threshold for line sensors to be red
#define redMax 600                        //maximum threshold for line sensors to be red
#define tunnelTime 500                   //time for the rest of the robot to go through the tunnel
#define turningTime 10000                   //time for the robot to turn to face the line after the tunnel
#define passiveSpeed 150                  //default speed of both motors
#define rightAdjMax 15                    //maximum the right adjust variable can be

//Variables
volatile boolean started = false;         //tag to say if the robot has started
volatile uint8_t leftSpeed, rightSpeed;   //speed variables of the left and right motors
float rightAdjuster, adjusterPower;       //changes the speed of the right wheel, exponent used during calculation of next rightAdjuster
volatile boolean forwardDirection = true; //says whether the robot is moving forward or backward
volatile int redLines = 0;                //the number of red lines robot has encountered
volatile boolean onBlack;                 //says if the robot is ona black are of the table
volatile boolean goStraight = false;      //tag to go straight when on a black area of the table
unsigned long whiteLine = 0;              //records time of when the robot came across a white line
volatile boolean amberValue;              //holds the value of the amber LED 
unsigned long amberLast;                  //hold value of millis() last time amber led changed
int usduration;                           // value received from ultrasound sensor
int ldr_value;                            // value read from LDR
int pos = 0;                              // denotes servo position
float f_distance;                         // robot-to-fruit distance at front
volatile boolean sensordoor_open = false; // says the sensor door is open or not
volatile boolean basketdoor_open = false; // says the basket door is open or not


void setup() {
  //set up serial and AFMS with default frequency
  Serial.begin(9600);
  AFMS.begin();
  amberValue = false;
  pinMode(amberled, OUTPUT);
  digitalWrite(amberled, LOW);

  pinMode(redled, OUTPUT);
  digitalWrite(redled, LOW);
  pinMode(greenled, OUTPUT);
  digitalWrite(greenled, LOW);
  pinMode(whiteled, OUTPUT);
  digitalWrite(whiteled, LOW);
  
  //Connect button interrupts
  attachInterrupt(digitalPinToInterrupt(startswitch), startFunc, RISING);
  attachInterrupt(digitalPinToInterrupt(resetswitch), resetFunc, RISING);
  //initalise adjusterPower
  adjusterPower = 3.0;

  //for testing without buttons ~
  //startFunc();

  sensor_servo.attach(9);                 // attaches the sensor door servo on pin 9
  basket_servo.attach(10);                // attaches the basket door servo on pin 10
  // initial position: sensor door at front, basket door closing the basket (both closed)

  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);
}

void loop() {
  //if the robot has started then do things
  if (started) {
    lineFollow();
    //fruit_collection();
    if (millis() - amberLast >= 250) {
      amberLast = millis();
      amberValue = amberValue ^ 1;
      digitalWrite(amberled, amberValue);
    }
    //digitalWrite(whiteled, goStraight);
  }
}

void startFunc() {
  //intialise some variables
  forwardDirection = true;
  started = true;
  rightAdjuster = 0;
  //moves forward to edge of box
  //leftSpeed = passiveSpeed;
  //rightSpeed = passiveSpeed;
  
  goStraight = true;
  redLines = 0;
}

void resetFunc() {
  //reset some variables
  leftSpeed = 0;
  rightSpeed = 0;
  leftMotor->setSpeed(leftSpeed);
  leftMotor->run(RELEASE);
  rightMotor->setSpeed(rightSpeed);
  rightMotor->run(RELEASE);
  forwardDirection = true;
  //stop amber light
  amberValue = false;
  digitalWrite(amberled, amberValue);
  started = false;
}

boolean isRed(int value) {
  //returns  boolean dependant on if the sensor value is in red range
  if (value >= redMin && value <= redMax) {
    return true;
  } else {
    return false;
  }
}

boolean isWhite(int value) {
  //returns  boolean dependant on if the sensor value is in white range
  if (value <= redMin) {
    return true;
  } else {
    return false;
  }
}

boolean isBlack(int value) {
  //returns  boolean dependant on if the sensor value is in black range
  if (value >= redMax) {
    return true;
  } else {
    return false;
  }
}

void lineFollow() {
  //reads the line sensors
  int left, mid, right;
  left = analogRead(ls1);
  mid = analogRead(ls2);
  right = analogRead(ls3);

  Serial.print(left);
  Serial.print(", ");
  /*Serial.print(mid - left);
  Serial.println(", ");*/
  Serial.print(mid);
  Serial.print(", ");
  Serial.print(right);
  Serial.println("");
  
  //checks which sensors are active in different ways
  if (isBlack(left) && isWhite(mid) && isBlack(right)) {
    //going straight BWB
    moveForward();
    goStraight = false;
    onBlack = false;
    digitalWrite(redled, LOW);
    digitalWrite(greenled, LOW);
  } else if(isBlack(left) && isBlack(mid) && isBlack(right)) {
    digitalWrite(redled, HIGH);
    /*delay(500);
    digitalWrite(redled, LOW);*/
    //on black ground BBB
    onBlack = true;
    if (goStraight){
      if ((millis() > whiteLine + tunnelTime) && (millis() < whiteLine + tunnelTime + turningTime)) {
        //need to turn to get back on track after going through the tunnel
        turnRight();
        //digitalWrite(redled, HIGH);        
      } else {
        //just need to go straight forward/backward
        moveForward();
        //digitalWrite(redled, LOW);
      }
    } else {
      //turn left as most likely, the robot went off on the right
      turnLeft();
    }
  } else if (isRed(left) && isRed(mid) && isRed(right)) {
    //straight across red line, front and back of fruit box RRR
    //Serial.println(redLines);
    if (onBlack == true) {
      redLines += 1;
      if (redLines == 2) {
        leftMotor->setSpeed(0);
        rightMotor->setSpeed(0);
        leftMotor->run(RELEASE);
        rightMotor->run(RELEASE);
        leftMotor->run(BACKWARD);
        rightMotor->run(FORWARD);
        forwardDirection = false;
        leftSpeed = passiveSpeed;
        rightSpeed = passiveSpeed;
        leftMotor->setSpeed(leftSpeed);
        rightMotor->setSpeed(rightSpeed);
        //open the door so that the fruit can leave!!!!!! ~
        for (pos = 0; pos <= 55; pos += 1) {  // ~~
        basket_servo.write(pos);
        }
      } else if (redLines == 3) {
        leftSpeed = passiveSpeed;
        rightSpeed = passiveSpeed;
        leftMotor->setSpeed(leftSpeed);
        rightMotor->setSpeed(rightSpeed);
        //adjuest time delay for reversing ~
        delay(5000);
      }
    }
    onBlack = false;
  } else if (isBlack(left) && isWhite(mid) && isWhite(right)) {
    //finished loop, need to get back into tunnel BWW
    /*leftMotor->setSpeed(250);
    rightMotor->setSpeed(50);
    //adjust turning delay ~
    delay(1000);*/
    digitalWrite(whiteled, HIGH);
    turnRight();
    //delay(3000);
  } else if (isWhite(left) && isWhite(mid) && isWhite(right)) {
    digitalWrite(greenled, HIGH);
    /*delay(1000);
    digitalWrite(greenled, LOW);*/
    //Straight across line - at start and just out of tunnel WWW
    //if (onBlack) {
      whiteLine = millis();
      onBlack = false;
    //}
    if (forwardDirection) {
      goStraight = true;
      //moveForward();
      turnRight();
    } else {
      turnRight();
    }
    delay(200);
  } else if (isWhite(left) && isBlack(mid) && isBlack(right)) {
    //need to turn left WBB
    turnLeft();
  } else if (isBlack(left) && isBlack(mid) && isWhite(right)) {
    //need to turn right BBW
    turnRight();
  } else if (isRed(left)) {
    //if approaching fruit box at an angle R__ 
    turnLeft();
  } else if (isRed(right)) {
    //if approaching fruit box at an angle _R
    turnRight();
  } else if (isWhite(left) && isWhite(mid) && isBlack(right)) {
    //Drifting to the right WWB
    turnLeft();
  } else {
    //nothing else should happen so print the values for debugging
    Serial.print(left);
    Serial.print(", ");
    Serial.print(mid);
    Serial.print(", ");
    Serial.print(right);
  }
  
}

void moveForward() {
  digitalWrite(whiteled, LOW);
  //function to move the robot forward
  if ((leftSpeed != passiveSpeed) || (rightSpeed != passiveSpeed)) {
      leftSpeed = passiveSpeed;
      rightSpeed = passiveSpeed;
      //rightMotor->run(BACKWARD);
      leftMotor->setSpeed(leftSpeed + 9);
      rightMotor->setSpeed(rightSpeed);
   }
   rightAdjuster = 0;
}

void turnLeft() {
  digitalWrite(whiteled, LOW);
  //function to turn the robot left
  //it works by increasing the speed of the right wheel
  /*incRightAdj();
  if (rightSpeed + rightAdjuster > 255) {
    rightSpeed = 255;
  } else {
    rightSpeed = rightSpeed + rightAdjuster;
  }
  leftSpeed = passiveSpeed;
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);*/
  if ((leftSpeed != 40) || (rightSpeed != 150)) {
      leftSpeed = 40;
      rightSpeed = 150;
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
   }
}

void turnRight() {
  //function to turn the robot right
  //it works by decreasing the speed of the right wheel
  /*incRightAdj();
  if (rightSpeed - rightAdjuster < 0) {
    rightSpeed = 0;
  } else {
    rightSpeed = rightSpeed - rightAdjuster;
  }
  leftSpeed = passiveSpeed;
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);*/
  if ((leftSpeed != 150) || (rightSpeed != 40)) {
      leftSpeed = 150;
      rightSpeed = 40;
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
   }
}

void incRightAdj() {
  //increase value of right adjuster
  if (rightAdjuster < rightAdjMax) {
    rightAdjuster += 1/pow(rightAdjuster + 1, adjusterPower);
  }
}

void fruit_collection() {
  // read ultrasound sensor (distance)
  noInterrupts();
  digitalWrite(ustrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(ustrig, LOW);
  usduration = pulseIn(usecho, HIGH);
  interrupts();
  f_distance = usduration * 0.034/2;

  if (0 < f_distance <= 5.2) {            // there is a fruit within 5 cm
    
    // stop the robot
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);

    // light up amber LED for 1 sec
    digitalWrite(amberled, HIGH);
    delay(1000);
    digitalWrite(amberled, LOW);
    
    // detect colour
    ldr_value = analogRead(ldr);
    if (ldr_value <= 500) {               // blue block -> unripe fruit
      // red LED >= 5 sec
      digitalWrite(redled, HIGH);
      delay(6000);
      digitalWrite(redled, LOW);
      // open sensor door to 90 degrees to avoid unripe fruit, keep basket door closed
      for (pos = 0; pos <= 90; pos += 1) {  // ~ position does not match actual angle
        sensor_servo.write(pos);
      }
      sensordoor_open = true;
    } else {  // red block -> ripe fruit
      // green LED >= 5 sec
      digitalWrite(greenled, HIGH);
      delay(6000);
      digitalWrite(greenled, LOW);
      // open both doors and align them to collect the fruit
      for (pos = 0; pos <= 35; pos += 1) {  // ~
        sensor_servo.write(pos);
      }
      for (pos = 0; pos <= 55; pos += 1) {  // ~
        basket_servo.write(pos);
      }
      sensordoor_open = true;
      basketdoor_open = true;
    }

    // collect or avoid the fruit
    if (sensordoor_open == true && basketdoor_open == false) { // drive past the fruit
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
      delay(1000);                          // ~ need to test how long should the robot move
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(0);
      // close the sensor door
      for (pos = 90; pos <= 0; pos -= 1) {  // ~
        sensor_servo.write(pos);
      }
      sensordoor_open = false;
    } else if (sensordoor_open == true && basketdoor_open == true) {  //collect the fruit
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
      delay(1000);                          // ~
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(0);
      // close both doors
      for (pos = 35; pos <= 0; pos -= 1) {  // ~
        sensor_servo.write(pos);
      }
      for (pos = 55; pos <= 0; pos -= 1) {  // ~
        basket_servo.write(pos);
      }
      sensordoor_open = false;
      basketdoor_open = false;
    } else if (sensordoor_open == false && basketdoor_open == false) { // continue following the line
      // !!!!!!! continue line following !!!!!!!~ (call line following function?)
      lineFollow();
    }
  }
}
