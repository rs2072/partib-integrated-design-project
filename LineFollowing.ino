//Libraries
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

//Set shield to default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

//Set up left and right motor
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

//Pins
#define ldr A0
#define ls1 A1 //left line sensor
#define ls2 A2 //middle line sensor
#define ls3 A3 //right line sensor
#define ustrig 3
#define usecho 2
#define startswitch 6
#define resetswitch 7
#define greenled 8
#define redled 11
#define amberled 12
#define whiteled 13

//Other constants
#define redMin 400                        //minimum threshold for line sensors to be red
#define redMax 600                        //maximum threshold for line sensors to be red
#define tunnelTime 2000                   //time for the rest of the robot to go through the tunnel
#define turningTime 500                   //time for the robot to turn to face the line after the tunnel
#define passiveSpeed 130                  //default speed of both motors
#define rightAdjMax 15                    //maximum the right adjust variable can be

//Variables
volatile boolean started = false;         //tag to say if the robot has started
volatile uint8_t leftSpeed, rightSpeed;   //speed variables of the left and right motors
float rightAdjuster, adjusterPower;       //changes the speed of the right wheel, exponent used during calculation of next rightAdjuster
volatile boolean forwardDirection = true; //says whether the robot is moving forward or backward
volatile int redLines = 0;                //the number of red lines robot has encountered
volatile boolean onBlack;                 //says if the robot is ona black are of the table
volatile boolean goStraight = false;      //tag to go straight when on a black area of the table
unsigned long whiteLine = 0;              //records time of when the robot came across a white line
volatile boolean amberValue;              //holds the value of the amber LED 
unsigned long amberLast;                  //hold value of millis() last time amber led changed


void setup() {
  //set up serial and AFMS with default frequency
  Serial.begin(9600);
  AFMS.begin();
  amberValue = false;
  pinMode(amberled, OUTPUT);
  digitalWrite(amberled, LOW);
  //Connect button interrupts
  attachInterrupt(digitalPinToInterrupt(startswitch), startFunc, RISING);
  attachInterrupt(digitalPinToInterrupt(resetswitch), resetFunc, RISING);
  //initalise adjusterPower
  adjusterPower = 3.0;

  //for testing without buttons ~
  startFunc();
}

void loop() {
  //if the robot has started then do things
  if (started) {
    lineFollow();
    if (millis() - amberLast >= 250) {
      amberLast = millis();
      amberValue = amberValue ^ 1;
      digitalWrite(amberled, amberValue);
    }
  }
}

void startFunc() {
  //intialise some variables
  forwardDirection = true;
  started = true;
  rightAdjuster = 0;
  //moves forward to edge of box
  leftSpeed = passiveSpeed;
  rightSpeed = passiveSpeed;
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);
  goStraight = true;
  redLines = 0;
}

void resetFunc() {
  //reset some variables
  leftSpeed = 0;
  rightSpeed = 0;
  leftMotor->setSpeed(leftSpeed);
  leftMotor->run(RELEASE);
  rightMotor->setSpeed(rightSpeed);
  rightMotor->run(RELEASE);
  forwardDirection = true;
  //stop amber light
  amberValue = false;
  digitalWrite(amberled, amberValue);
  started = false;
}

boolean isRed(int value) {
  //returns  boolean dependant on if the sensor value is in red range
  if (value >= redMin && value <= redMax) {
    return true;
  } else {
    return false;
  }
}

boolean isWhite(int value) {
  //returns  boolean dependant on if the sensor value is in white range
  if (value >= redMax) {
    return true;
  } else {
    return false;
  }
}

boolean isBlack(int value) {
  //returns  boolean dependant on if the sensor value is in black range
  if (value <= redMin) {
    return true;
  } else {
    return false;
  }
}

void lineFollow() {
  //reads the line sensors
  int left, mid, right;
  left = analogRead(ls1);
  mid = analogRead(ls2);
  right = analogRead(ls3);
  //checks which sensors are active in different ways
  if (isBlack(left) && isWhite(mid) && isBlack(right)) {
    //going straight BWB
    moveForward();
    goStraight = false;
    onBlack = false;
  } else if(isBlack(left) && isBlack(mid) && isBlack(right)) {
    //on black ground BBB
    onBlack = true;
    if (goStraight){
      if ((millis() > whiteLine + tunnelTime) && (millis() < whiteLine + tunnelTime + turningTime)) {
        //need to turn to get back on track after going through the tunnel
        turnRight();        
      } else {
        //just need to go straight forward/backward
        moveForward();
      }
    } else {
      //turn left as most likely, the robot went off on the right
      turnLeft();
    }
  } else if (isRed(left) && isRed(mid) && isRed(right)) {
    //straight across red line, front and back of fruit box RRR
    if (onBlack == true) {
      redLines += 1;
      if (redLines == 2) {
        leftMotor->setSpeed(0);
        rightMotor->setSpeed(0);
        leftMotor->run(RELEASE);
        rightMotor->run(RELEASE);
        leftMotor->run(BACKWARD);
        rightMotor->run(BACKWARD);
        forwardDirection = false;
        leftSpeed = passiveSpeed;
        rightSpeed = passiveSpeed;
        leftMotor->setSpeed(leftSpeed);
        rightMotor->setSpeed(rightSpeed);
        //open the door so that the fruit can leave!!!!!! ~
      } else if (redLines == 3) {
        leftSpeed = passiveSpeed;
        rightSpeed = passiveSpeed;
        leftMotor->setSpeed(leftSpeed);
        rightMotor->setSpeed(rightSpeed);
        //adjuest time delay for reversing ~
        delay(5000);
      }
    }
    onBlack = false;
  } else if (isBlack(left) && isWhite(mid) && isWhite(right)) {
    //finished loop, need to get back into tunnel BWW
    leftMotor->setSpeed(250);
    rightMotor->setSpeed(50);
    //adjust turning delay ~
    delay(1000);
  } else if (isWhite(left) && isWhite(mid) && isWhite(right)) {
    //Straight across line - at start and just out of tunnel WWW
    if (onBlack) {
      whiteLine = millis();
      onBlack = false;
    }
    if (forwardDirection) {
      goStraight = true;
      moveForward();
    } else {
      turnRight();
    }
  } else if (isWhite(left) && isBlack(mid) && isBlack(right)) {
    //need to turn left WBB
    turnLeft();
  } else if (isBlack(left) && isBlack(mid) && isWhite(right)) {
    //need to turn right BBW
    turnRight();
  } else if (isRed(left)) {
    //if approaching fruit box at an angle R__ 
    turnLeft();
  } else if (isRed(right)) {
    //if approaching fruit box at an angle _R
    turnRight();
  } else if (isWhite(left) && isWhite(mid) && isBlack(right)) {
    //Drifting to the right WWB
    turnLeft();
  } else {
    //nothing else should happen so print the values for debugging
    Serial.print(left);
    Serial.print(", ");
    Serial.print(mid);
    Serial.print(", ");
    Serial.print(right);
  }
  
}

void moveForward() {
  //function to move the robot forward
  if ((leftSpeed != passiveSpeed) || (rightSpeed != passiveSpeed)) {
      leftSpeed = passiveSpeed;
      rightSpeed = passiveSpeed;
      leftMotor->setSpeed(leftSpeed);
      rightMotor->setSpeed(rightSpeed);
   }
   rightAdjuster = 0;
}

void turnLeft() {
  //function to turn the robot left
  //it works by increasing the speed of the right wheel
  incRightAdj();
  if (rightSpeed + rightAdjuster > 255) {
    rightSpeed = 255;
  } else {
    rightSpeed = rightSpeed + rightAdjuster;
  }
  leftSpeed = passiveSpeed;
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);
}

void turnRight() {
  //function to turn the robot right
  //it works by decreasing the speed of the right wheel
  incRightAdj();
  if (rightSpeed - rightAdjuster < 0) {
    rightSpeed = 0;
  } else {
    rightSpeed = rightSpeed - rightAdjuster;
  }
  leftSpeed = passiveSpeed;
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);
}

void incRightAdj() {
  //increase value of right adjuster
  if (rightAdjuster < rightAdjMax) {
    rightAdjuster += 1/pow(rightAdjuster + 1, adjusterPower);
  }
}