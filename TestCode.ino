#define ldr A0
#define ls1 A1
#define ls2 A2
#define ls3 A3
#define ustrig 3
#define usecho 2
#define startswitch 6
#define resetswitch 7
#define greenled 8
#define redled 11
#define amberled 12
#define whiteled 13

int usduration;
volatile boolean started = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(startswitch), startfunc, RISING);
  attachInterrupt(digitalPinToInterrupt(resetswitch), resetfunc, RISING);

}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("Started: ");
  Serial.print(started);
  Serial.print("  LDR: ");
  Serial.print(analogRead(ldr));
  Serial.print("  LS1: ");
  Serial.print(analogRead(ls1));
  Serial.print("  LS2: ");
  Serial.print(analogRead(ls2));
  Serial.print("  LS3: ");
  Serial.print(analogRead(ls3));
  noInterrupts();
  digitalWrite(ustrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(ustrig, LOW);
  usduration = pulseIn(usecho, HIGH);
  interrupts();
  Serial.print("  Ultrasonic: ");
  Serial.print(usduration * 0.034/2);
  Serial.println("");
  digitalWrite(greenled, HIGH);
  delay(250);
  digitalWrite(greenled, LOW);
  digitalWrite(redled, HIGH);
  delay(250);
  digitalWrite(redled, LOW);
  digitalWrite(amberled, HIGH);
  delay(250);
  digitalWrite(amberled, LOW);
  digitalWrite(whiteled, HIGH);
  delay(250);
  digitalWrite(whiteled, LOW);
}

void startfunc() {
  started = true;
}
void resetfunc() {
  started = false;
}